///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file test.cpp
/// @version 1.0
///
/// Exports data about all palila
///
/// @author Shannon Kam <skam7@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   02_18_2021
///////////////////////////////////////////////////////////////////////////////
#include<iostream>
#include <array>
#include <list>

#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "fish.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "bird.hpp"
#include "palila.hpp"
#include "nene.hpp"
#include "node.hpp"
#include "list.hpp"

using namespace std;
using namespace animalfarm;

int main(){  
   cout << "Hello World" << endl; 
   Node node;
   SingleLinkedList list;
   
   cout << "Is it empty: " << boolalpha << list.empty() << endl;
   
   list.push_front(new Node());
   cout << "Add node" << endl;
   cout << "Is it empty: " << boolalpha << list.empty() << endl;
   list.push_front(new Node());
   cout << "Add another node" << endl;
   cout << "Number of elements: " << list.size() << endl;
   
   list.pop_front();
   cout << "Remove node" << endl;
   cout << "Number of elements: " << list.size() << endl;
   
   list.push_front(new Node());
   list.push_front(new Node());
   cout << "Added 2 nodes" << endl;
   cout << "Number of elements: " << list.size() << endl;  
   cout << "Is it empty: " << boolalpha << list.empty() << endl;
   return 0;
}
