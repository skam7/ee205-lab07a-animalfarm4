///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file animal.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Shannon Kam <skam7@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   03_02_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>
#include <cstdlib>
#include <random>

#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"
#include "animal.hpp"

using namespace std;

namespace animalfarm {

static random_device rd;
static mt19937_64 RNG( rd() );
static bernoulli_distribution boolRNG( 0.5 );

Animal::Animal(){
   cout << "." ;
}

Animal::~Animal(){
   cout << "x" ;
}

void Animal::printInfo() {
   cout << "   Species = [" << species << "]" << endl;
	cout << "   Gender = [" << genderName( gender ) << "]" << endl;
}


string Animal::genderName (enum Gender gender) {
   switch (gender) {
      case MALE:    return string("Male"); break;
      case FEMALE:  return string("Female"); break;
      case UNKNOWN: return string("Unknown"); break;
   }

   return string("Really, really Unknown");
};

const Gender Animal::getRandomGender(){
   if( boolRNG( RNG ))
      return MALE;
   else
      return FEMALE;
}

string Animal::colorName (enum Color color) {
	/// @todo Implement this based on genderName and your work
	///       on Animal Farm 1
   switch(color){
      case BLACK: return string("Black"); break;
      case WHITE: return string("White"); break;
      case RED: return string("Red"); break;
      case SILVER: return string("Silver"); break;
      case YELLOW: return string("Yellow"); break;
      case BROWN: return string("Brown"); break;
   } 
   return string("Unknown");
};

const enum Color Animal::getRandomColor(){
   uniform_int_distribution<> colorRNG(BLACK, BROWN);
   int r = colorRNG( RNG );
   switch(r){
      case 0: return BLACK; break;
      case 1: return WHITE; break;
      case 2: return RED; break;
      case 3: return SILVER; break;
      case 4: return YELLOW; break;
      case 5: return BROWN; break;
   }
   return BLACK;
}

const bool Animal::getRandomBool(){
   return boolRNG( RNG );
}

const float Animal::getRandomWeight( const float from, const float to ){
   uniform_real_distribution<> floatRNG(from, to);
   float f = floatRNG( RNG );

   return f;
}

const string Animal::getRandomName(){
   int length = rd()%6 + 4;
   int i;

   string name;

   static const char c_low[] = "abcdefghijklmnopqrstuvwxyz";
   static const char c_up[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

   name = c_up[rd()%26];
   for( i = 0; i < length; i++){
      name = name + c_low[rd()%26];
   }
   return name;
}

Animal* AnimalFactory::getRandomAnimal(){
   Animal* newAnimal = NULL;
   int i = rd()%6;
   switch(i){
      case 0: newAnimal = new Cat (Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender()); break;
      case 1: newAnimal = new Dog (Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender()); break;
      case 2: newAnimal = new Nunu (Animal::getRandomBool(), RED, Animal::getRandomGender()); break;
      case 3: newAnimal = new Aku (Animal::getRandomWeight(5, 21), SILVER, Animal::getRandomGender()); break;
      case 4: newAnimal = new Palila (Animal::getRandomName(), YELLOW, Animal::getRandomGender()); break;
      case 5: newAnimal = new Nene (Animal::getRandomName(), BROWN, Animal::getRandomGender()); break;
   }
   
   return newAnimal;
}

} // namespace animalfarm
