///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file list.hpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Shannon Kam <skam7@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   03_23_2021
///////////////////////////////////////////////////////////////////////////////
#pragma once

using namespace std;

#include <string>
#include "node.hpp"

namespace animalfarm {

class SingleLinkedList {
   protected:
      Node* head = nullptr;
      unsigned int amntNodes = 0;

   public:
      const bool empty() const;
      void push_front(Node* newNode);
      Node* pop_front();
      Node* get_first() const;
      Node* get_next(const Node* currentNode) const;
      unsigned int size() const;
   
};

} // namespace animalfarm
