///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file list.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Shannon Kam <skam7@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   03_23_2021
///////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <string>
#include <cstdlib>

#include "list.hpp" 
using namespace std;

namespace animalfarm {

//return true if list is empty
const bool SingleLinkedList::empty() const{ 
   if( head == nullptr )
      return true;
   else
      return false;
   
}

//add newNode to front of list
void SingleLinkedList::push_front(Node* newNode){
   if(newNode == nullptr)
      return;
   newNode->next = head;
   head = newNode;
   amntNodes++;
}

//remove node from the front, if list is empty return nullptr
Node* SingleLinkedList::pop_front(){
   if( head == nullptr )
      return nullptr;
   Node* returns = head;
   head = head->next;
   amntNodes--;
   return returns;
}

//return first node from the list, don't make changes to the list
Node* SingleLinkedList::get_first() const{
   return head;
}

//return node immediately following currentNode
Node* SingleLinkedList::get_next(const Node* currentNode) const{
   return currentNode->next;
}

//return number of nodes in list
unsigned int SingleLinkedList::size() const{
   return amntNodes;
}
   
}
