###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 07a - Animal Farm 4
#
# @file    Makefile
# @version 1.0
#
# @author Shannon Kam <skam7@hawaii.edu>
# @brief  Lab 07a - Animal Farm 4 - EE 205 - Spr 2021
# @date   02_18_2021
###############################################################################

all: main test

main.o:  animal.hpp main.cpp
	g++ -c main.cpp

animal.o: animal.hpp animal.cpp
	g++ -c animal.cpp
	
mammal.o: mammal.hpp mammal.cpp
	g++ -c mammal.cpp

fish.o: fish.cpp fish.hpp
	g++ -c fish.cpp

bird.o: bird.cpp bird.hpp
	g++ -c bird.cpp

cat.o: cat.cpp cat.hpp
	g++ -c cat.cpp

dog.o: dog.cpp dog.hpp
	g++ -c dog.cpp

nunu.o: nunu.cpp nunu.hpp
	g++ -c nunu.cpp

aku.o: aku.cpp aku.hpp
	g++ -c aku.cpp

palila.o: palila.cpp palila.hpp
	g++ -c palila.cpp

nene.o: nene.cpp nene.hpp
	g++ -c nene.cpp

test.o: test.cpp
	g++ -c test.cpp

node.o: node.cpp node.hpp
	g++ -c node.cpp

list.o: list.cpp list.hpp
	g++ -c list.cpp

main: main.cpp *.hpp main.o animal.o mammal.o fish.o bird.o cat.o dog.o nunu.o aku.o palila.o nene.o node.o list.o
	g++ -o main main.o animal.o mammal.o fish.o bird.o cat.o dog.o nunu.o aku.o palila.o nene.o node.o list.o

test: test.cpp *.hpp test.o animal.o mammal.o fish.o bird.o cat.o dog.o nunu.o aku.o palila.o nene.o node.o list.o
	g++ -o test test.o animal.o mammal.o fish.o bird.o cat.o dog.o nunu.o aku.o palila.o nene.o node.o list.o

clean:
	rm -f *.o main test
