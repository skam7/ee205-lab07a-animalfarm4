///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Shannon Kam <skam7@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   03_02_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <array>
#include <list>

#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"
#include "node.hpp"
#include "list.hpp"

using namespace std;
using namespace animalfarm;

int main() {
   cout << "Welcome to Animal Farm 4" << endl;
   /////////////////////////////// ///////////////////////////////
   /////////////////////////////// Animal List ///////////////////////////////
   /////////////////////////////// ///////////////////////////////
   
   SingleLinkedList animalList;
   
   for(auto i = 0; i < 25; i++){
      animalList.push_front(AnimalFactory::getRandomAnimal());
   }
   
   cout << endl;
   cout << "List of Animals: " << endl;
   cout << "   Is it empty: " << boolalpha << animalList.empty() << endl;
   cout << "   Number of Elements: " << animalList.size() << endl;
   
   for(auto animal = animalList.get_first(); animal != nullptr; animal = animalList.get_next(animal)){
      cout << ((Animal*)animal)->speak() << endl;
   }
    
   while(!animalList.empty()){
      Animal* animal = (Animal*) animalList.pop_front();
      
      delete animal;
   }
   return 0;
}

